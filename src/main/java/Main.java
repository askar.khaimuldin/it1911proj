import domain.User;
import repositories.entities.UserRepository;
import repositories.interfaces.IEntityRepository;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
//        try {
//            String connStr = "jdbc:postgresql://localhost:5432/myapp";
//            Connection conn = DriverManager.getConnection(connStr, "postgres", "0000");
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery("SELECT * FROM users");
//            while(rs.next()) {
//                System.out.println(rs.getString("name") + " " +
//                        rs.getString("surname"));
//            }
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }

        IEntityRepository userRepository = new UserRepository();
        User user = new User("Shokan", "Ualikhanov", "shokan7",
                "Qwertysddv!456", Date.valueOf("1864-03-15"));
        userRepository.add(user);
    }
}
