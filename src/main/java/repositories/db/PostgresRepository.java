package repositories.db;

import repositories.interfaces.IDBRepository;

import java.sql.*;

public class PostgresRepository implements IDBRepository {
    @Override
    public Connection getConnection() {
        try {
            String connStr = "jdbc:postgresql://localhost:5433/myapp";
            Connection conn = DriverManager.getConnection(connStr, "postgres", "0000");
            return conn;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
