package repositories.entities;

import domain.User;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IEntityRepository;

import javax.xml.stream.events.StartDocument;
import java.sql.SQLException;
import java.sql.Statement;

public class UserRepository implements IEntityRepository<User> {
    private IDBRepository dbrepo;

    public UserRepository() {
        dbrepo = new PostgresRepository();
    }

    @Override
    public void add(User entity) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            String sql = "INSERT INTO users(name, surname, username, password, birthday) " +
                    "VALUES('" + entity.getName() + "','"+ entity.getSurname() +
                    "','"+ entity.getUsername() +"','"+ entity.getPassword() +
                    "','"+ entity.getBirthday() +"')";
            stmt.execute(sql);
        } catch(SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void remove(User entity) {

    }

    @Override
    public Iterable<User> query(String sql) {
        return null;
    }
}
